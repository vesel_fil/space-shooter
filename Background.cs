﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    class Background : GameObject
    {

        private Texture2D Texture { get; set; }

        public int Width { get { return Texture.Width; } }
        public int Height { get { return Texture.Height; } }

        private float Speed { get; set; }
        private Vector2[] Stars { get; set; }

        public Background(int width, int height, Game game) : base(game)
        {
            Texture = new Texture2D(game.GraphicsDevice, width, height);
            Stars = new Vector2[500];

            for(int i = 0; i < Stars.Length; i++)
            {
                Stars[i] = Maths.GenerateRandomVector(width, height);
            }

            Speed = 5f;
        }

        public override void Update(GameTime time, KeyboardState keyState, MouseState mouseState)
        {
            for(int i = 0; i < Stars.Length; i++)
            {
                Stars[i] += new Vector2(0, Speed);
                if (Stars[i].Y > Height)
                    Stars[i] -= new Vector2(0, Height);
            }

            base.Update(time, keyState, mouseState);
        }

        public override void Render(SpriteBatch batch)
        {
            Color[] texData = new Color[Width * Height];
            for (int i = 0; i < texData.Length; i++)
                texData[i] = Color.Black;

            foreach (Vector2 star in Stars)
                if(star.X < Width && star.Y < Height)
                    texData[(int)star.X + (int)star.Y * Width] = Color.White;

            Texture.SetData(texData);

            batch.Draw(Texture, new Vector2(0), null);

            base.Render(batch);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

    }
}
