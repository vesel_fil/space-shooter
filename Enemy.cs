﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LudumGame
{
    abstract class Enemy : Entity
    {
        private Shield Shield { get; set; }

        public Texture2D ProjectileTexture { get; set; }

        public Vector2 PositionToReach { get; set; }

        public int GunCooldown;

        public int GunCooldownLength = 20;

        public int Health = 100;

        public Enemy(Level level, Vector2 position) : base(level)
        {
            Position = position;
            PositionToReach = position;
            GunCooldown = 10;
        }

        public override void Initialize(ContentManager content)
        {
            ProjectileTexture = content.Load<Texture2D>("red-projectile");
            Scale = 0.5f;
            Effects = SpriteEffects.FlipVertically;

            Shield = new Shield(Level, ShieldTypes.ENEMY);
            AddChild(Shield);

            Shield.Init(content);
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
        }

        public override void Tick(InputBundle input)
        {
            int speed = 5;

            if (!IsInSpot(PositionToReach, speed * 2))
            {
                if (PositionToReach.X < Position.X)
                {
                    if (Math.Abs(PositionToReach.X - Position.X) > speed)
                        Position += new Vector2(-speed, 0);
                    else
                        Position += new Vector2(-1, 0);
                }
                else
                {
                    if (Math.Abs(Position.X - PositionToReach.X) > 3)
                        Position += new Vector2(speed, 0);
                    else
                        Position += new Vector2(1, 0);
                }

                if (PositionToReach.Y < Position.Y)
                {
                    if (Math.Abs(PositionToReach.Y - Position.Y) > speed)
                        Position += new Vector2(0, -speed);
                    else
                        Position += new Vector2(0, -1);
                }
                else
                {
                    if (Math.Abs(Position.Y - PositionToReach.Y) > speed)
                        Position += new Vector2(0, speed);
                    else
                        Position += new Vector2(0, 1);
                }
            }

            GunCooldown--;
            LookAt(Level.Player);
        }

        public void Shoot(Vector2 target)
        {
            if (GunCooldown > 0)
                return;
            if (ProjectileTexture == null)
                throw new Exception("You have to the projectile texture to something in order to shoot!");

            Projectile proj = new Projectile(Level, ProjectileDirection.DIRECTION_DOWN);
            AddChild(proj);

            proj.Texture = ProjectileTexture;
            proj.Init(null);
            
            proj.Target = target;

            GunCooldown = GunCooldownLength;
        }

        public void Shoot()
        {
            Shoot(new Vector2(-1, -1));
        }

        public bool IsInSpot(Vector2 p, float tolerance)
        {
            return p.X > (Position.X - tolerance) && p.X < (Position.X + tolerance) 
                && p.Y > (Position.Y - tolerance) && p.Y < (Position.Y + tolerance);
        }

        public void GoTo(Vector2 position)
        {
            PositionToReach = position;
        }

        public void Hit(int damage)
        {
            if(Shield != null)
            {
                if (Shield.IsActive)
                {
                    Shield.Hit(damage);
                    return;
                }
            }
            Health -= damage;

            if (Health <= 0)
                ShouldBeRemoved = true;
        }


        public List<Projectile> GetProjectiles()
        {
            List<Projectile> projectiles = new List<Projectile>();

            foreach(GameObject child in Children)
            {
                if (child is Projectile)
                    projectiles.Add((Projectile)child);
            }

            return projectiles;
        }
    }
}
