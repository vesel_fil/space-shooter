﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace LudumGame
{
    class EnemyWave : GameObject
    {

        public List<Enemy> Enemies { get; set; }

        public Level Level { get; set; }

        private Vector2 ShootingPosition { get; set; }

        private int ShootingEnemyIndex;

        private int EnemyShootoutLength = 75;

        private int EnemyShootoutCounter;

        public EnemyWave(Level level) : base(level.Game)
        {
            Level = level;
            Enemies = new List<Enemy>();
            ShootingEnemyIndex = 0;
            EnemyShootoutCounter = 0;
        }

        public override void Init(ContentManager content)
        {
            base.Init(content);
        }

        public override void Update(GameTime time, KeyboardState keyState, MouseState mouseState)
        {
            ShootingPosition = new Vector2(Level.Player.Position.X, 100);

            if(ShootingEnemyIndex < Enemies.Count)
                Enemies[ShootingEnemyIndex].GoTo(ShootingPosition);

            foreach (Enemy e in Enemies)
            {
                if (e.IsInSpot(ShootingPosition, 20))
                    e.Shoot();

                foreach(Projectile p in Level.Player.GetProjectiles())
                {
                    if(e.Collides(p))
                    {
                        p.ShouldBeRemoved = true;
                        e.Hit(20);
                    }
                }
            }

            EnemyShootoutCounter--;
            if(EnemyShootoutCounter < 0)
            {
                EnemyShootoutCounter = EnemyShootoutLength;

                Console.WriteLine(ShootingEnemyIndex);
                ShootingEnemyIndex = ShootingEnemyIndex < (Enemies.Count - 1) ? ShootingEnemyIndex + 1 : 0;
                if(ShootingEnemyIndex < Enemies.Count)
                    Enemies[ShootingEnemyIndex].GoTo(ShootingPosition);

                for (int i = 0; i < Enemies.Count; i++)
                {
                    if (i == ShootingEnemyIndex)
                        continue;

                    Enemies[i].GoTo(Maths.GenerateRandomVector(Level.ViewportWidth - 100, 300) + new Vector2(50, 0));
                }
            }

            for (int i = 0; i < Enemies.Count; i++)
                if (Enemies[i].ShouldBeRemoved)
                    Enemies.Remove(Enemies[i]);
               
            base.Update(time, keyState, mouseState);
        }

        public void AddEnemy(Enemy e)
        {
            AddChild(e);
            Enemies.Add(e);
        }
    }
}
