﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    class EnemyWaveGenerator
    {

        /*public EnemyWave GenerateFromFile(String path, Level level)
        {

        }*/

        public static EnemyWave GenerateEnemyWave(int easyCount, int normalCount, int hardCount, Level level)
        {
            if ((easyCount + normalCount + hardCount) > 8)
                throw new Exception("You cannot have more than 8 enemies!");
            EnemyWave enemyWave = new EnemyWave(level);

            int lastColPos = 0;
            int lastRowPos = 0;

            for (int i = 0; i < easyCount; i++)
            {
                enemyWave.AddEnemy(new EasyEnemy(level, new Vector2(100 + 100 * lastColPos,
                    100 + 100 * lastRowPos), level.EnemyTextures["easy-enemy"]));
                lastColPos++;
                if (lastColPos > level.ViewportWidth)
                {
                    lastColPos = 0;
                    lastRowPos = 1;
                }
            }

            for (int i = 0; i < normalCount; i++)
            {
                enemyWave.AddEnemy(new NormalEnemy(level, new Vector2(50 + 100 * lastColPos,
                    50 + 100 * lastRowPos), level.EnemyTextures["normal-enemy"]));

                if (lastColPos > level.ViewportWidth)
                {
                    lastColPos = 0;
                    lastRowPos = 1;
                }
            }

            for (int i = 0; i < hardCount; i++)
            {
                enemyWave.AddEnemy(new HardEnemy(level, new Vector2(50 + 100 * lastColPos,
                    50 + 100 * lastRowPos), level.EnemyTextures["hard-enemy"]));

                if (lastColPos > level.ViewportWidth)
                {
                    lastColPos = 0;
                    lastRowPos = 1;
                }
            }

            return enemyWave;
        }

    }
}
