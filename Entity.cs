﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LudumGame
{
    abstract class Entity : GameObject
    {
        public Texture2D Texture { get; set; }

        public Vector2 Position { get; set; }

        public Vector2 Center
        {
            get
            {
                Vector2 center = new Vector2(Position.X, Position.Y);
                center += new Vector2(Texture.Width / 2, Texture.Height / 2);

                return center;
            }
        }

        public Vector2 Origin {
            get
            {
                if (Texture == null)
                    return new Vector2(1, 1);
                return new Vector2(Texture.Width / 2, Texture.Height / 2);
            }
        }

        public float Rotation { get; set; }

        public float RotationDeg
        {
            get
            {
                return Maths.ToDegrees(Rotation);
            }

            set
            {
                Rotation = Maths.ToRadians(value);
            }
        }

        public float Scale { get; set; }

        public float Depth { get; set; }

        public SpriteEffects Effects { get; set; }

        public Level Level { get; set; }

        public int Width { get {
                if (Texture != null)
                    return (int)(Texture.Width * Scale);
                else return 0;
            }
        }

        public int Height { get {
                if (Texture != null)
                    return (int)(Texture.Height * Scale);
                else return 0;
            }
        }

        public Entity ParentEntity
        {
            get
            {
                if (typeof(Entity).IsAssignableFrom(Parent.GetType()))
                    return ((Entity)Parent);
                else throw new Exception("Parent is not of type Entity!");
            }
        }

        public Entity(Level level) : base(level.Game)
        {
            Level = level;

            Effects = SpriteEffects.None;
            Scale = 1.0f;
            Rotation = 0.0f;
            Depth = 0.0f;
        }

        public Entity(Vector2 position, Level level) : this(level)
        {
            Position = position;
        }

        public Entity(Vector2 pos, Texture2D tex, Level level) : this(pos, level)
        {
            Texture = tex;
        }

        public override void Render(SpriteBatch batch)
        {
            base.Render(batch);

            Draw(batch);
        }

        public override void Update(GameTime time, KeyboardState keyState, MouseState mouseState)
        {
            Tick(new InputBundle(keyState, mouseState, time));
            base.Update(time, keyState, mouseState);
        }

        public override void Init(ContentManager content)
        {
            base.Init(content);
            Initialize(content);
        }

        public virtual void Draw(SpriteBatch batch)
        {
            if (Texture == null || Position == null)
            {
                Console.WriteLine("One of the rendering parameters are null!");
                return;
            }
            //Default rendering, very simple, should be overriden in case of shader usage or something simmilar
            batch.Draw(Texture, Position, null, Color.White, Rotation, Origin, Scale, Effects, Depth);
        }

        public Rectangle GetCollisionBox()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
        }

        public bool Collides(Entity other)
        {
            return GetCollisionBox().Intersects(other.GetCollisionBox());
        }

        public bool IsParentEntity()
        {
            if (Parent == null)
                return false;
            else return typeof(Entity).IsAssignableFrom(Parent.GetType());
        }

        public void LookAt(Vector2 pos)
        {
            float deltaX = Position.X - pos.X;
            float deltaY = Position.Y - pos.Y;

            Rotation = (float)Math.Atan2(deltaY, deltaX) + (float)Math.PI / 2.0f;
        }

        public void LookAt(Entity e)
        {
            LookAt(e.Position);
        }

        public abstract void Tick(InputBundle input);
        public abstract void Initialize(ContentManager content);
    }
}
