﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LudumGame
{
    class GameObject : IGameComponent
    {

        public GameObject Parent { get; set; }
        public List<GameObject> Children { get; set; }

        public Game Context { get; set; }

        public bool ShouldBeRemoved { get; set; }

        public GameObject(Game game)
        {
            Context = game;
            Children = new List<GameObject>();
            ShouldBeRemoved = false;
        }

        public void AddChild(GameObject obj)
        {
            Children.Add(obj);
            obj.Parent = this;
        }

        public virtual void Dispose()
        {
            ShouldBeRemoved = true;
            foreach (GameObject child in Children)
                child.Dispose();
        }

        public virtual void Init(ContentManager content)
        {
            foreach (GameObject child in Children)
                child.Init(content);
        }

        public virtual void Render(SpriteBatch batch)
        {
            foreach (GameObject child in Children)
                child.Render(batch);
        }

        public virtual void Update(GameTime time, KeyboardState keyState, MouseState mouseState)
        {
            List<GameObject> toBeRemoved = new List<GameObject>();
            foreach (GameObject child in Children)
            {
                child.Update(time, keyState, mouseState);
                if (child.ShouldBeRemoved)
                    toBeRemoved.Add(child);
            }

            foreach (GameObject i in toBeRemoved)
                Children.Remove(i);
        }
    }
}
