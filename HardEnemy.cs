﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    class HardEnemy : Enemy
    {

        public HardEnemy(Level level, Vector2 position, Texture2D texture) : base(level, position)
        {
            Texture = texture;
        }

    }
}
