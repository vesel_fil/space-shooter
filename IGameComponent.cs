﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    interface IGameComponent
    {

        void Update(GameTime time, KeyboardState keyState, MouseState mouseState);
        void Render(SpriteBatch batch);
        void Dispose();
        void Init(ContentManager content);

    }
}
