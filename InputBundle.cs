﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    public class InputBundle
    {

        public KeyboardState Keyboard { get; set; }
        public MouseState Mouse { get; set; }

        public GameTime Time { get; set; }

        public InputBundle(KeyboardState k, MouseState m, GameTime gt)
        {
            Keyboard = k;
            Mouse = m;
            Time = gt;
        }

    }
}
