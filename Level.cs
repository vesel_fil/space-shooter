﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LudumGame
{
    class Level : IGameComponent
    { 
        public Background Background { get; set; }
        public Player Player { get; set; }

        public KeyboardState KeyState { get; private set; }
        public MouseState Mouse { get; private set; }
        public GameTime Time { get; private set; }

        public Game Game { get; private set; }

        public int ViewportWidth { get; private set; }
        public int ViewportHeight { get; private set; }

        public Rectangle Viewport { get; private set; }

        public GameObject RootObject { get; private set; }

        public Dictionary<string, Texture2D> EnemyTextures { get; set; }

        public EnemyWave Enemies;

        public Level(int viewportWidth, int viewportHeight, Game game)
        {
            Game = game;
            ViewportHeight = viewportHeight;
            ViewportWidth = viewportWidth;

            Viewport = new Rectangle(0, 0, ViewportWidth, ViewportHeight);

            RootObject = new GameObject(game);
            EnemyTextures = new Dictionary<string, Texture2D>();
        }

        public void Dispose()
        {

        }

        public void Init(ContentManager content)
        {
            /*EnemyTextures.Add("hard-enemy", content.Load<Texture2D>("hard-enemy"));
            EnemyTextures.Add("normal-enemy", content.Load<Texture2D>("normal-enemy"));
            EnemyTextures.Add("easy-enemy", content.Load<Texture2D>("easy-enemy"));*/

            Background = new Background(ViewportWidth, ViewportHeight, Game);
            RootObject.AddChild(Background);

            Player = new Player(this);
            RootObject.AddChild(Player);

            Enemies = EnemyWaveGenerator.GenerateEnemyWave(5, 0, 0, this);
            RootObject.AddChild(Enemies);
            /*RootObject.AddChild(Enemies);
            for(int i = 0; i < 5; i++)
            {
                Enemies.AddEnemy(new EasyEnemy(this, new Vector2(100 + i * 200, 100), EnemyTextures["easy-enemy"]));
            }*/

            RootObject.Init(content);
        }

        public void Render(SpriteBatch batch)
        {
            RootObject.Render(batch);
        }

        public void Update(GameTime time, KeyboardState keyState, MouseState mouseState)
        {
            //Update timing and input values
            KeyState = keyState;
            Mouse = mouseState;
            Time = time;

            foreach(Enemy e in Enemies.Enemies)
            {
                foreach(Projectile p in e.GetProjectiles())
                {
                    if (p.Collides(Player))
                    {
                        p.ShouldBeRemoved = true;
                        Player.Hit(20);
                    }
                }
                    
            }

            RootObject.Update(time, keyState, mouseState);
        }
    }
}
