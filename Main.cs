﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LudumGame
{
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Level level;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 800;
            graphics.PreferredBackBufferWidth = 1280;

            graphics.ApplyChanges();
        }

        protected override void Initialize()
        {
            base.Initialize();
            int widthBorder = (GraphicsDevice.DisplayMode.Width - graphics.PreferredBackBufferWidth) / 2;
            int heightBorder = (GraphicsDevice.DisplayMode.Height - graphics.PreferredBackBufferHeight) / 2;

            Window.Position = new Point(widthBorder, heightBorder);

        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            level = new Level(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, this);
            level.EnemyTextures.Add("hard-enemy", Content.Load<Texture2D>("hard-enemy"));
            level.EnemyTextures.Add("easy-enemy", Content.Load<Texture2D>("easy-enemy"));
            level.EnemyTextures.Add("normal-enemy", Content.Load<Texture2D>("normal-enemy"));

            level.Init(Content);
        }

        protected override void UnloadContent()
        {
            level.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            KeyboardState keyState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            level.Update(gameTime, keyState, mouseState);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            level.Render(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
