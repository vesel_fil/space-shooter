﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LudumGame
{
    class Maths
    {
        private static Random rng;

        private static Random Rng { get
            {
                if (rng != null)
                    return rng;
                else
                {
                    rng = new Random();
                    return Rng;
                }
            }
        }

        public static Vector2 GenerateRandomVector(int maxX, int maxY)
        {
            return new Vector2(Rng.Next(maxX), Rng.Next(maxY));
        }

        public static Vector2 GenerateRandomVector(int max)
        {
            return GenerateRandomVector(max, max);
        }

        public static int GetRandomNumber(int min, int max)
        {
            return Rng.Next(min, max);
        }

        public static int GetRandomNumber(int max)
        {
            return GetRandomNumber(0, max);
        }

        public static int GetRandomNumber()
        {
            return GetRandomNumber(int.MaxValue);
        }

        //TODO: check this
        public static float ToDegrees(float rad)
        {
            return (float)(rad * (180 / Math.PI));
        }

        public static float ToRadians(float deg)
        {
            return (float)((deg / 180) * Math.PI);
        }

    }
}
