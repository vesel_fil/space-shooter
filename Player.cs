﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace LudumGame
{
    class Player : Entity
    {

        private int shootingCooldown;
        private Texture2D projectileTexture;

        private Shield Shield { get; set; }

        private int Health { get; set; }

        private ProgressBar ShieldBar { get; set; }

        private ProgressBar HealthBar { get; set; }

        public Player(Level level) : base(level)
        {
            Health = 100;
        }

        public override void Initialize(ContentManager content)
        {
            Texture = content.Load<Texture2D>("player");
            projectileTexture = content.Load<Texture2D>("blue-projectile");
            Scale = 0.5f;

            int xPos = (Level.ViewportWidth - (Width / 2)) / 2;
            Position = new Vector2(xPos, Level.ViewportHeight - (10 + Height));

            Shield = new Shield(Level, ShieldTypes.PLAYER);
            AddChild(Shield);
            Shield.Init(content);

            Shield.Scale = 1f;

            HealthBar = new ProgressBar(new Vector2(40, Level.ViewportHeight - 50), Level.Game, ProgressBarColors.RED);
            ShieldBar = new ProgressBar(new Vector2(80, Level.ViewportHeight - 50), Level.Game, ProgressBarColors.BLUE);

            AddChild(HealthBar);
            AddChild(ShieldBar);
        }

        public override void Tick(InputBundle input)
        {
            Shield.Scale = 0.35f;

            KeyboardState key = input.Keyboard;
            if (key.IsKeyDown(Keys.A) || key.IsKeyDown(Keys.Left))
                Position += new Vector2(-5, 0);
            if (key.IsKeyDown(Keys.D) || key.IsKeyDown(Keys.Right))
                Position += new Vector2(5, 0);
            if (shootingCooldown < 0)
            {
                shootingCooldown = 60;
                Shoot();
            }

            shootingCooldown--;
            HealthBar.Progress = Health;
            ShieldBar.Progress = Shield.Status / 5;
        }

        public void Shoot(Vector2 target)
        {
            if (projectileTexture == null)
                throw new Exception("You have to the projectile texture to something in order to shoot!");

            Projectile proj = new Projectile(Level, ProjectileDirection.DIRECTION_UP);
            AddChild(proj);

            proj.Texture = projectileTexture;
            proj.Init(null);
            proj.Speed = 20;

            proj.Target = target;
        }

        public void Shoot()
        {
            Shoot(new Vector2(-1, -1));
        }

        public void Hit(int damage)
        {
            if (Shield != null)
            {
                if (Shield.IsActive)
                {
                    Shield.Hit(damage);
                    return;
                }
            }
            Health -= damage;
        }

        public List<Projectile> GetProjectiles()
        {
            List<Projectile> projectiles = new List<Projectile>();

            foreach (GameObject child in Children)
            {
                if (child is Projectile)
                    projectiles.Add((Projectile)child);
            }

            return projectiles;
        }
    }
}
