﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LudumGame
{
    class ProgressBar : GameObject
    {

        public float Progress { get; set; }

        public ProgressBarColors Color { get; set; }

        public Texture2D Texture { get; set; }

        public Vector2 Position { get; set; }

        public float Scale { get; set; }

        public ProgressBar(Vector2 pos, Game game, ProgressBarColors color) : base(game)
        {
            Position = pos;
            Scale = 0.2f;
            Color = color;
        }

        public override void Init(ContentManager content)
        {
            if (Color == ProgressBarColors.BLUE)
                Texture = content.Load<Texture2D>("health-bar");
            else
                Texture = content.Load<Texture2D>("shield-bar");
        }

        public override void Render(SpriteBatch batch)
        {
            if (Progress > 100)
                Progress = 100;
            if (Texture == null)
                return;

            batch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, (int)(Texture.Height * ((float)Progress / 100.0))), 
                Microsoft.Xna.Framework.Color.White, (float)Math.PI, 
                new Vector2((Texture.Width * Scale) / 2, (Texture.Height * Scale) / 2), Scale, SpriteEffects.None, 0.5f);

            base.Render(batch);
        }

    }

    public enum ProgressBarColors
    {
        RED, BLUE
    }
}
