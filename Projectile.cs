﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LudumGame
{
    class Projectile : Entity
    {

        private ProjectileDirection Direction;

        public Vector2 Target { get; set; }

        private bool HasRotationBeenSet { get; set; }

        public int Speed { get; set; }

        public Projectile(Level level, ProjectileDirection dir) : base(level)
        {
            Direction = dir;
            HasRotationBeenSet = false;
            Speed = 10;
        }

        public override void Initialize(ContentManager content)
        {
            Entity parentEntity;
            try
            {
                parentEntity = (Entity)Parent;
            } catch
            {
                throw new Exception("Projectiles parent needs to be type of Entity!");
            } 
            Scale = 0.1f;

            Position = parentEntity.Position;
            Position += new Vector2(0, Direction == ProjectileDirection.DIRECTION_DOWN ?
                parentEntity.Height / 2 : -parentEntity.Height / 2);

            if (Direction == ProjectileDirection.DIRECTION_DOWN)
                Effects = SpriteEffects.FlipVertically;
        }

        public override void Tick(InputBundle input)
        {
            if(Target == null || Target.Equals(new Vector2(-1, -1)))
            {
                if (Direction == ProjectileDirection.DIRECTION_DOWN)
                {
                    Position += new Vector2(0, Speed);
                }
                else
                {
                    Position += new Vector2(0, -Speed);
                }
            } else
            {
                if(!HasRotationBeenSet)
                    LookAt(Target);

                HasRotationBeenSet = true;
                Position += new Vector2((float)Math.Cos(Rotation + (float)Math.PI / 2) * Speed,
                    (float)Math.Sin(Rotation + (float)Math.PI / 2) * Speed);
            }
            

            if (!Level.Viewport.Intersects(GetCollisionBox()))
                Dispose();
        }
    }

    public enum ProjectileDirection {
        DIRECTION_DOWN, DIRECTION_UP
    }
}
