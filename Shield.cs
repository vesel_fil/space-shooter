﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LudumGame
{
    class Shield : Entity
    {

        public Rectangle TargetRectangle { get; set; }

        public bool UpdateSizeEachTick { get; set; }

        public ShieldTypes Type { get; set; }

        public int Status { get; set; }

        public int Intensity { get; set; }


        public bool IsActive
        {
            get
            {
                return Status > 0;
            }
        }

        public Shield(Level level, ShieldTypes type) : base(level)
        {
            UpdateSizeEachTick = false;
            Type = type;

            Status = type == ShieldTypes.ENEMY ? 100 : 500;
        }

        public override void Initialize(ContentManager content)
        {
            if(!IsParentEntity())
                throw new Exception("Parent cannot be null and must be type of Entity!");

            Position = ParentEntity.Position - new Vector2(20, 20);
            TargetRectangle = new Rectangle((int)Position.X, (int)Position.Y, 
                ParentEntity.Width + 40, ParentEntity.Height + 40);

            Texture = content.Load<Texture2D>(Type == ShieldTypes.ENEMY ? "red-shield" : "blue-shield");
            Scale = 0.3f;
        }

        public override void Tick(InputBundle input)
        {
            if (UpdateSizeEachTick)
            {
                TargetRectangle = new Rectangle((int)Position.X, (int)Position.Y, 
                    ParentEntity.Width + 40, ParentEntity.Height + 40);
            }

            Position = ParentEntity.Position;
            if (Intensity > 0)
                Intensity -= 2;
        }

        public void Hit(int damage)
        {
            Intensity += damage * 5;
            Status -= damage;
        }

        public override void Draw(SpriteBatch batch)
        {
            if (Status <= 0 || Intensity <= 0)
                return;

            batch.Draw(Texture, Position, null, Color.White * (Intensity / 100.0f), 
                Rotation, Origin, Scale, Effects, Depth);
        }
    }
    
    public enum ShieldTypes
    {
        ENEMY, PLAYER
    }
}
